﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqareCalc
{
    public static class Globals
    {
        public static readonly Int32 CNST_CIRCLE = 1; // circyle
        public static readonly Int32 CNST_TRIANGLE = 2; // triangle
    }

    public abstract class Figure
    {
        protected int type = 0;
        abstract public double Area();
        public Int32 GetFigureType()
        {
            return type;
        }
    }

    public class Circle : Figure
    {
        double Radius = 0;

        public Circle(double _Radius)
        {
            Radius = _Radius;
            type = Globals.CNST_CIRCLE;
        }

        public override double Area()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }

    public class Triangle : Figure
    {
        double[] sides = new double[] { 0, 0, 0};

        public Triangle(double side1, double side2, double side3)
        {
            sides[0] = side1;
            sides[1] = side2;
            sides[2] = side3;
            type = Globals.CNST_TRIANGLE;
        }
        public override double Area()
        {
            if (sides[0] + sides[1] > sides[2] && sides[0] + sides[2] > sides[1] && sides[1] + sides[2] > sides[0])
            {
                double P = sides.Sum() / 2;
                return Math.Sqrt(P * (P - sides[0]) * (P - sides[1]) * (P - sides[2]));
            }
            else
                return -1;            
        }

        public bool RightTriangle()
        {
            return (Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2) == Math.Pow(sides[2], 2) ||
                (Math.Pow(sides[0], 2) + Math.Pow(sides[2], 2) == Math.Pow(sides[1], 2)) ||
                (Math.Pow(sides[2], 2) + Math.Pow(sides[1], 2) == Math.Pow(sides[0], 2)));
        }
    }
}
