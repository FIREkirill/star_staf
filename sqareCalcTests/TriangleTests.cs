﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using sqareCalc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sqareCalc.Tests
{
    [TestClass()]
    public class TriangleTests
    {
        [TestMethod()]
        public void CircyleTest()
        {
            Figure Figures = new Circle(10);
                     
            Assert.AreEqual(Figures.GetFigureType(), Globals.CNST_CIRCLE);
            Assert.AreNotEqual(Figures.Area(), 0);       
        }

        [TestMethod()]
        public void TriangleTest()
        {
            Figure Figures = new Triangle(3, 10, 7);

            Assert.AreEqual(Figures.GetFigureType(), Globals.CNST_TRIANGLE);
            Assert.AreNotEqual(Figures.Area(), 0);
        }

        [TestMethod()]
        public void RightTriangleTest()
        {
            Figure Figures = new Triangle(6, 6, 7);

            Assert.AreEqual(Figures.GetFigureType(), Globals.CNST_TRIANGLE);
            Assert.AreNotEqual(((Triangle)Figures).RightTriangle(), true);
            Assert.AreNotEqual(Figures.Area(), 0);
        }
   
        [TestMethod()]
        public void MultiFigureTest()
        {
            var Figures = new List<Figure>
            {
                new Circle(10),
                new Triangle(3, 6, 7),
            };

            foreach (var Fig in Figures)
            {
                if (Fig.GetFigureType() == Globals.CNST_TRIANGLE)
                {
                    ((Triangle)Fig).RightTriangle();
                }

                Assert.AreNotEqual(Fig.Area(), 0);
            }
        }
    }
}